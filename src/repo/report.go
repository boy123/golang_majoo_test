package repo

import (
	"golang_majoo_test/src/entity"

	"gorm.io/gorm"
)

type ReportRepository interface {
	ReportSatu(UserID int) ([]entity.Report, error)
}

type reportRepository struct {
	db *gorm.DB
}

func NewReportRepository(db *gorm.DB) *reportRepository {
	return &reportRepository{db}
}

func (r *reportRepository) ReportSatu(UserID int) ([]entity.Report, error) {
	var report []entity.Report

	r.db.Raw("SELECT report.merchant_name, report.omzet, report.date FROM ( SELECT u.merchant_name, sum( u.bill_total ) AS omzet, date FROM ( SELECT adddate(( SELECT min( ? ) FROM Transactions ), ROW - 1 ) AS date FROM ( SELECT @ROW := @ROW + 1 AS ROW FROM ( SELECT 0 UNION ALL SELECT 1 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 ) t,(SELECT 0 UNION ALL SELECT 1 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 ) t2, ( SELECT @ROW := 0 ) r ) n WHERE n.ROW <= ( SELECT datediff( max( ? ), min( ? )) FROM Transactions ) + 1 ) dr LEFT JOIN ( SELECT b.merchant_name, a.bill_total AS bill_total, DATE( a.created_at ) AS tanggal FROM Transactions AS a INNER JOIN Merchants AS b ON a.merchant_id = b.id WHERE b.user_id = ? ) u ON dr.date = u.tanggal GROUP BY dr.date ) report LIMIT 0,10", "2021-11-01", "2021-11-30", "2021-11-01", 1).Select("merchant_name, omzet, date").Find(&report)

	return report, nil
}
