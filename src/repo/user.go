package repo

import (
	"golang_majoo_test/src/entity"

	"gorm.io/gorm"
)

type UserRepository interface {
	CekLogin(username string, password string) (entity.User, error)
	FindByID(ID int) (entity.User, error)
}

type userRepository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *userRepository {
	return &userRepository{db}
}

func (r *userRepository) CekLogin(username string, password string) (entity.User, error) {
	var user entity.User

	err := r.db.Where(map[string]interface{}{"user_name": username, "password": password}).Find(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil

}

func (r *userRepository) FindByID(ID int) (entity.User, error) {
	var user entity.User

	err := r.db.Where("id = ?", ID).Find(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}
