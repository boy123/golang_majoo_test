package service

import (
	"errors"
	"golang_majoo_test/src/entity"
	"golang_majoo_test/src/helper"
	"golang_majoo_test/src/input"
	"golang_majoo_test/src/repo"
)

type Service interface {
	Login(input input.LoginInput) (entity.User, error)
	GetUserByID(ID int) (entity.User, error)
}

type service struct {
	repository repo.UserRepository
}

func NewService(repository repo.UserRepository) *service {
	return &service{repository}
}

func (s *service) Login(input input.LoginInput) (entity.User, error) {
	username := input.Username
	password := input.Password

	user, err := s.repository.CekLogin(username, helper.GetMD5Hash(password))
	if err != nil {
		return user, err
	}

	if user.ID == 0 {
		return user, errors.New("Username atau password kamu salah")
	}

	return user, nil

}

func (s *service) GetUserByID(ID int) (entity.User, error) {
	user, err := s.repository.FindByID(ID)
	if err != nil {
		return user, err
	}

	if user.ID == 0 {
		return user, errors.New("No user found on with that ID")
	}

	return user, nil
}
