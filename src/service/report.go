package service

import (
	"golang_majoo_test/src/entity"
	"golang_majoo_test/src/repo"
)

type ReportService interface {
	GetReportSatu(UserID int) ([]entity.Report, error)
}

type serviceReport struct {
	repository repo.ReportRepository
}

func NewReportService(repository repo.ReportRepository) *serviceReport {
	return &serviceReport{repository}
}

func (s *serviceReport) GetReportSatu(UserID int) ([]entity.Report, error) {
	report, err := s.repository.ReportSatu(UserID)
	if err != nil {
		return report, err
	}

	return report, nil
}
