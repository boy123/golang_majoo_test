package helper

import "golang_majoo_test/src/entity"

type UserFormatter struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	UserName string `json:"username"`
	Token    string `json:"token"`
}

func FormatUser(user entity.User, token string) UserFormatter {
	formatter := UserFormatter{
		ID:       user.ID,
		Name:     user.Name,
		UserName: user.UserName,
		Token:    token,
	}

	return formatter
}

type ReportSatuFormatter struct {
	MerchantName string `json:"nama_merchant"`
	Omzet        int64  `json:"omzet"`
	Date         string `json:"tanggal"`
}

func FormatReportSatu(report entity.Report) ReportSatuFormatter {
	reportFormatter := ReportSatuFormatter{}
	merchantNotNull := report.MerchantName
	if len(reportFormatter.MerchantName) == 0 {
		reportFormatter.MerchantName = merchantNotNull
	} else {
		reportFormatter.MerchantName = report.MerchantName
	}
	reportFormatter.Omzet = report.Omzet
	reportFormatter.Date = report.Date

	return reportFormatter
}

func FormatReportSatus(reports []entity.Report) []ReportSatuFormatter {
	reportsFormatter := []ReportSatuFormatter{}

	for _, report := range reports {
		reportFormatter := FormatReportSatu(report)
		reportsFormatter = append(reportsFormatter, reportFormatter)
	}

	return reportsFormatter
}
