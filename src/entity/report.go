package entity

type Report struct {
	MerchantName string
	Omzet        int64
	Date         string
}

type Pagination struct {
	Limit int    `json:"limit"`
	Page  int    `json:"page"`
	Sort  string `json:"sort"`
}
