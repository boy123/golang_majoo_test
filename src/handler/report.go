package handler

import (
	"golang_majoo_test/src/entity"
	"golang_majoo_test/src/helper"
	"golang_majoo_test/src/input"
	"golang_majoo_test/src/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type reportHandler struct {
	reportService service.ReportService
}

func NewReportHandler(reportService service.ReportService) *reportHandler {
	return &reportHandler{reportService}
}

func (h *reportHandler) DownloadReportSatu(c *gin.Context) {
	var input input.ReportInput

	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.APIResponse("Failed to create report", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	currentUser := c.MustGet("currentUser").(entity.User)

	downloadReport, err := h.reportService.GetReportSatu(currentUser.ID)
	if err != nil {
		errorMessage := gin.H{"errors": err.Error()}

		response := helper.APIResponse("download failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	response := helper.APIResponse("Success to create report", http.StatusOK, "success", helper.FormatReportSatus(downloadReport))
	c.JSON(http.StatusOK, response)

}
