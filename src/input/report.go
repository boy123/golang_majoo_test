package input

import "golang_majoo_test/src/entity"

type ReportInput struct {
	From string `json:"from" form:"from" binding:"required"`
	To   string `json:"to" form:"to" binding:"required"`
	User entity.User
}
