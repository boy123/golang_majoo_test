package main

import (
	"golang_majoo_test/src"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {

	env := godotenv.Load()
	if env != nil {
		log.Fatal("Error on loading .env file")
		return
	}

	host := os.Getenv("HOST")
	userHost := os.Getenv("USER_HOST")
	userPass := os.Getenv("USER_PASS")
	databaseName := os.Getenv("DATABASE_NAME")
	databasePort := os.Getenv("DATABASE_PORT")

	dsn := userHost + ":" + userPass + "@tcp(" + host + ":" + databasePort + ")/" + databaseName + "?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal(err.Error())
	}

	router := src.SetUpRouter(db)

	router.Run()

}
